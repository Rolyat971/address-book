<?php

namespace AppBundle\Manager;


use AppBundle\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;

class CompanyManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    protected function getRepository()
    {
        return $this->em->getRepository(Company::class);
    }

    public function get($id)
    {
        return $this->getRepository()->find($id);
    }

    public function getList()
    {
        return $this->getRepository()->findAll();
    }
}
