<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\Company;
use AppBundle\Manager\CompanyManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CompanyControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();

        $companies = [
            (new Company())->setId(1)->setName('BIM'),
        ];

        $mamagerMock = $this->prophesize(CompanyManager::class);
        $mamagerMock->getList()
            ->willReturn($companies)
            ->shouldBeCalledTimes(1)
        ;

        $client->getContainer()->set(CompanyManager::class, $mamagerMock->reveal());

        $crawler = $client->request('GET', '/company/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertCount(
            1,
            $crawler->filter('table tbody tr')
        );
    }

    public function testShow()
    {
        $client = static::createClient();

        $company = (new Company())->setId(1)->setName('BIM');

        $mamagerMock = $this->prophesize(CompanyManager::class);
        $mamagerMock->getList()->willReturn([]);
        $mamagerMock->get(1)
            ->willReturn($company)
            ->shouldBeCalledTimes(1)
        ;

        $client->getContainer()->set(CompanyManager::class, $mamagerMock->reveal());

        $crawler = $client->request('GET', '/company/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(
            'BIM',
            $crawler->filter('h2')->text()
        );
    }

}
