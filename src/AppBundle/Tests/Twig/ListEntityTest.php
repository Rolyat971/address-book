<?php

namespace AppBundle\Tests\Twig;

use AppBundle\Entity\Company;
use AppBundle\Entity\Contact;
use AppBundle\Manager\CompanyManager;
use AppBundle\Repository\ContactRepository;
use AppBundle\Twig\Extension\ListEntity;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ListEntityTest extends WebTestCase
{
    public function testListEntity()
    {
        $client = static::createClient();

//        $companies = [
//            (new Company())->setId(1)->setName('BIM'),
//        ];
//
//        $mamagerMock = $this->prophesize(CompanyManager::class);
//        $mamagerMock->getList()
//            ->willReturn($companies)
//        ;
//
//        $client->getContainer()->set(CompanyManager::class, $mamagerMock->reveal());

        $crawler = $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertCount(
            2,
            $crawler->filter('dropdown-menu cmp')
        );
    }

}
