<?php

namespace AppBundle\Twig\Extension;


use AppBundle\Manager\CompanyManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class ListEntity extends \Twig_Extension
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Router
     */
    protected $router;

    public function __construct(Session $session, ContainerInterface $container , Router $router)
    {
        $this->session = $session;
        $this->container = $container;
        $this->router = $router;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('listEntity', [ $this, 'listEntity' ], [
                'is_safe' => ['html' => true]
            ]),
        ];
    }

    public function getManager($entity)
    {
        return $this->container->get(sprintf('AppBundle\Manager\%sManager', ucwords($entity)));
    }

    public function listEntity($entity)
    {
        $html = '';
        $list = $this->getManager($entity)->getList();


        foreach ($list as $item){
            $route = $this->router->generate(sprintf('app_%s_show', $entity), ['id' => $item->getId()]);
            $name = $item->getName();
            $html .= <<<HTML
<a class="dropdown-item cmp" href="$route">$name</a> 
HTML;
        }

        return $html;

    }
}
